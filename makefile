CC=		gcc
CFLAGS=		-Wall -std=gnu99
LIBS=		-lm
SOURCE=		$(wildcard *.c)
PROGRAMS=	${SOURCE:.c=}
OBJECTS=	${SOURCE:.c=.o} 
TARGET=		main

all:	${TARGET}	

${TARGET}: ${OBJECTS} 
	${CC} ${CFLAGS} -o $@ $^ ${LIBS}

board.o: board.c board.h
	${CC} ${CFLAGS} -c board.c ${LIBS}

%.o:%.c
	${CC} ${CFLAGS} -c $^ ${LIBS}

clean:
	rm -f ${PROGRAMS}
	rm -f ${OBJECTS}

