/*	priority_queue.h

	Header file for queue implementation

	by: Steven Skiena
*/

/*
Copyright 2003 by Steven S. Skiena; all rights reserved. 

Permission is granted for use in non-commerical applications
provided this copyright notice remains intact and unchanged.

This program appears in my book:

"Programming Challenges: The Programming Contest Training Manual"
by Steven Skiena and Miguel Revilla, Springer-Verlag, New York 2003.

See our website www.programming-challenges.com for additional information.

This book can be ordered from Amazon.com at

http://www.amazon.com/exec/obidos/ASIN/0387001638/thealgorithmrepo/

*/


#include "board.h"
#ifndef PQ_SIZE
#define PQ_SIZE       1000

typedef struct {
        struct board* q[PQ_SIZE+1];		/* body of queue */
        int n;                          /* number of queue elements */
} priority_queue;
void pq_init(priority_queue *q);
int pq_parent(int n);
int pq_young_child(int n);
void bubble_up(priority_queue *q, int p);
void bubble_down(priority_queue *q, int p);
int pq_insert(priority_queue *q, struct board *x);
void pq_swap(priority_queue *q, int i, int j);
struct board* extract_min(priority_queue *q);
int empty_pq(priority_queue *q);
void print_pq(priority_queue *q);
void free_pq(priority_queue *q);




#endif

