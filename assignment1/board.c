#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "board.h"

//global vars
bool debug = false;

int boardRate(struct board b){
	int rating = 0;
	for(int i = 1; i <= BOARD_SIZE; i++){
		rating += mDist(b, i);
	}
	return rating;
}

int mDist(struct board b,int value){
	int i =0;
	for(i = 0; i<BOARD_SIZE;i++){
		if(b.tiles[i] == value){
			break;
		}	
	}
	int dist = 0;
	if(i == value +1){
		return 0;
	}
	switch(value){
		case 1:
			switch(i){
				case 9:
					dist +=1;
				case 6:
				case 8:
					dist +=1;
				case 7:
				case 5: 
				case 3:
					dist +=1;
				case 1:
				case 4:
					dist +=1;
				case 0:
				break;
			}	
		break;
		case 2:
			switch(i){
				case 8: 
				case 6:
					dist +=1;
				case 3:
				case 5:
				case 7:
					dist +=1;
				case 0:
				case 2:
				case 4:
					dist +=1;
				case 1:
				break;	
			}
		break;
		case 3:
			switch(i){
				case 6:
					dist +=1;
				case 3:
				case 7:
					dist +=1;
				case 0:
				case 4:
				case 8:
					dist +=1;
				case 1:
				case 5:
					dist +=1;
				case 2:
				break;
			}
		break;
		case 4:
			switch(i){
				case 2:
				case 8:
					dist+=1;
				case 7:
				case 5:
				case 1:
					dist+=1;
				case 6: 
				case 4: 
				case 0: 
					dist+=1;
				case 3:
					break;
			}	
		break;
		case 5:
			switch(i){
				case 2:
				case 0:
				case 6:
				case 8:
					dist +=1;
				case 7:
				case 5:
				case 3:
				case 1:
					dist +=1;
				case 4:
					break;
			}
		break;
		case 6:
			switch(i){
				case 6:
				case 0:
					dist +=1;
				case 7:
				case 3:
				case 1:
					dist+=1;
				case 8:
				case 4:
				case 2:
					dist+=1;
				case 5:
					break;
			}
		break;
		case 7:
			switch(i){
				case 2:
					dist+=1;
				case 1:
				case 5:
					dist+=1;
				case 0:
				case 4:
				case 8:
					dist+=1;
				case 3:
				case 7:
					dist+=1;
				case 6:
					break;
			}	
		break;
		case 8:
			switch(i){
				case 0:
				case 2:
					dist+=1;
				case 1:
				case 3:
				case 5:
					dist+=1;
				case 4:
				case 6:
				case 8:
					dist+=1;
				case 7:
					break;
			}		
		break;
	}
	return dist;
}

//this checks struct boars for validity
bool structIsValid(struct board b){
	//In order to be valid, a state must contain all of the digits 0-8.
	bool contains[BOARD_SIZE] = {false,false,false,false,false,false,false,false,false};
//for each tile slot, mark the correspoinding index in contains[] as true
	for(int i =0; i<9; i++){
		contains[b.tiles[i]]=true;
	}
	//if all are true, then return true.  one false will return false.
	return(contains[0] && contains[1] && contains[2] && contains[3] && contains[4] && contains[5] && contains[6] && contains[7] && contains[8]);
}


//getChild allocates memory to the array of boards called children (i.e. children should be a pointer array).  don't forget to free it!
//Returns number of children
int getChildren(struct board *b, struct board **children, int avoidRepeats){
	int childrenCount = 0;
	int empty = 9;
//allocate memory for each potential child
	struct board* up = malloc(sizeof(struct board));
	struct board* down = malloc(sizeof(struct board));
	struct board* left = malloc(sizeof(struct board));
	struct board* right = malloc(sizeof(struct board));
	//find index of 0 tile and also fill children tiles with parent tile data
	for(int i = 0; i < 9; i++){
		if(b->tiles[i] == 0){
			empty = i;
		}
		//simultaneously initialize data for the child nodes
		up->tiles[i] = b->tiles[i];
		down->tiles[i] = b->tiles[i];
		left->tiles[i] = b->tiles[i];
		right->tiles[i] = b->tiles[i];
	}
	//set their parent, so that we can track back.
	up->parent = b;
	down->parent = b;
	left->parent = b;
	right->parent = b;
	//set their last move, so that we can track it..
	up->move = 'u';
	down->move = 'd';
	left->move = 'l';
	right->move = 'r';

	//check if down is a valid move(i.e. index is not 6,7, or 8), and write it to the children array
	if(0 <= empty && empty < 6){
		//swap empty tile with tile below it
		down->tiles[empty] = down->tiles[empty+3];
		down->tiles[empty+3] = 0; 
		
		if(avoidRepeats && hasRepeats(down)){
			//we sholuldn't return this child
			free(down);
			down = NULL;
		}else{
			//isn't a repeat/aren't checking for it, so add it to the child list
			children[childrenCount] = down;
			//we have one confirmed child
			childrenCount++;
		}
	}else{
		//down isn't a valid move, so nullify and free down
		free(down);
		down = NULL;
	}
	//check if up is a valid move, and write it to the children array
	if(3 < empty && empty < 9){
		//swap empty tile with tile above it
		up->tiles[empty] = up->tiles[empty-3];
		up->tiles[empty-3] = 0; 
		if(avoidRepeats && hasRepeats(up)){
			//we sholuldn't return this child
			free(up);
			up = NULL;
		}else{
			//isn't a repeat/aren't checking for it, so add it to the child list
			children[childrenCount] = up;
			//we have one confirmed child
			childrenCount++;
		}
	}else{
		//up isn't a valid move, so nullify and free down
		free(up);
		up = NULL;
	}
	//check if right is a valid move, and write it to the children array
	if(empty% 3 != 2){
		//swap empty tile with tile to the right
		right->tiles[empty] = right->tiles[empty+1];
		right->tiles[empty+1] = 0; 

		if(avoidRepeats && hasRepeats(right)){
			//we sholuldn't return this child
			free(right);
			right = NULL;
		}else{
			//isn't a repeat/aren't checking for it, so add it to the child list
			children[childrenCount] = right;
			//we have one confirmed child
			childrenCount++;
		}
	
	}else{
		//right isn't a valid move, so nullify and free right 
		free(right);
		right = NULL;
	}
	//check if left is a valid move, and write it to the children array
	if(empty% 3 != 0){
		//swap empty tile with tile to the left
		left->tiles[empty] = left->tiles[empty-1];
		left->tiles[empty-1] = 0; 

		if(avoidRepeats && hasRepeats(left)){
			//we sholuldn't return this child
			free(left);
			left = NULL;
		}else{
			//isn't a repeat/aren't checking for it, so add it to the child list
			children[childrenCount] = left;
			//we have one confirmed child
			childrenCount++;
		}
	}else{
		//left isn't a valid move, so nullify and free left
		free(left);
		down = NULL;
	}
	return childrenCount;
}

//checks if the board is in a solved state
bool structIsSolved(struct board b){
	for(int i = 1; i<8;i++){
		//check the tiles numbered 1-8 against their target positions on the board
		if(i != b.tiles[i-1]){
			return false;
		}
		//check if the 0 tile is in the right spot
		if (b.tiles[8] != 0){
			return false;
		}
	}
	return true;
}

void printBoard(struct board b){
	for(int i =0; i < 9; i++){
		if(i%3 == 0){
			printf("[%d, ",b.tiles[i]);
		}else if(i%3  == 2){
			printf("%d]\n",b.tiles[i]);	
		}else{
			printf("%d, ",b.tiles[i]);
		}
	}
}

int boardEquals(struct board* a, struct board* b){
	for(int i = 0; i < BOARD_SIZE; i++){
		if((a->tiles)[i] != (b->tiles)[i]){
			return false;
		}
	}
	return true;
}

int hasRepeats(struct board *b){
	struct board* parent = b->parent;
	while(parent != NULL){
		if(boardEquals(parent, b)){
			return true;
		}
		parent = parent->parent;
	}
	return false;
}
