/*	queue.c
 *
	
	implementation of a FIFO queue abstract data type.

	by: Steven Skiena
	begun: March 27, 2002
*/


/*
Copyright 2003 by Steven S. Skiena; all rights reserved. 

Permission is granted for use in non-commerical applications
provided this copyright notice remains intact and unchanged.

This program appears in my book:

"Programming Challenges: The Programming Contest Training Manual"
by Steven Skiena and Miguel Revilla, Springer-Verlag, New York 2003.

See our website www.programming-challenges.com for additional information.

This book can be ordered from Amazon.com at

http://www.amazon.com/exec/obidos/ASIN/0387001638/thealgorithmrepo/

*/


#include "queue.h"
#include "board.h"


//i can't really comment on this code since we found it online
void init_queue(queue *q)
{
        q->first = 0;
        q->last = QUEUESIZE-1;
        q->count = 0;
}

int enqueue(queue *q, struct board* x)
{
        if (q->count >= QUEUESIZE){
		return -1;
        } else {
                q->last = (q->last+1) % QUEUESIZE;
                q->q[ q->last ] = x;    
                q->count = q->count + 1;
        }
	return 1;
}

struct board *dequeue(queue *q)
{
        struct board* x;

        if (q->count > 0)
        {
                x = q->q[ q->first ];
                q->first = (q->first+1) % QUEUESIZE;
                q->count = q->count - 1;
        }

        return(x);
}

int empty(queue *q)
{
        if (q->count <= 0) return (1);
        else return (0);
}

//print the contents of the queue
void print_queue(queue *q)
{
        int i;

        i=q->first; 
        
        while (i != q->last) {
                printBoard(*(q->q[i]));
                i = (i+1) % QUEUESIZE;
        }

        printBoard(*(q->q[i]));
        printf("\n");
}

//free all the nodes in the queue
void free_all(queue *q){
	while(! empty(q)){
		struct board* node = dequeue(q);
		free(node);
	}
	return;
}
