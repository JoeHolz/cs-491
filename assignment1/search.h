#include "board.h"
#ifndef SEARCH_H
#define SEARCH_H
void breadthFirst(struct board* initial, int avoidRepeats);
void bestFirst(struct board* initial);
void depthFirst(struct board* initial, int avoidRepeats);
#endif
