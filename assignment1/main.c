#include "board.h"
#include "search.h"
#include <unistd.h>

//Main just parses command line arguments and creates an array containing the integers it's fed.
int main(int argc, char *argv[]){
	//create default initial state board
	struct board initialState;
	initialState.parent = NULL;
	for(int i = 0; i < 9; i++){
		initialState.tiles[i] = -1;
	}

	int c;
	int i = 0;
	debug = 0;
	if ((c = getopt(argc, argv, "v")) != -1) {
	// Option argument
		switch (c) {
			case 'v': 
				debug = 1;
				break;
			default:
				break;
		}
	}
	while(optind < argc){
			initialState.tiles[i%9] = atoi(argv[optind]);
			i++;
			optind++;
	}
	//validate input
	if(structIsValid(initialState)){
		printf("initial state is valid:\n");
		printBoard(initialState);
		//check struct if it's in a solved state
		if(structIsSolved(initialState)){
			printf("initialState[9] is already in a solved state.\n");
		}else{
			//run the breadth search
			printf("\n----now breadth searching\n");
			breadthFirst(&initialState, 0);
			printf("\n----now breadth searching, with no repeats\n");
			breadthFirst(&initialState, 1);
			printf("\n----now best first searching\n");
			bestFirst(&initialState);
			printf("\n---Depth first\n");
			depthFirst(&initialState,0);
			printf("\n---Depth first, no repeats\n");
			depthFirst(&initialState,1);
			return EXIT_SUCCESS;
		}	
	}else{
		printf("initialState[9] is not valid, or -v was not specified before tile arguments \n");
		printf("exiting program.\n");
		exit(-1);
	}	
}
