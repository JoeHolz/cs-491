/*author: Robert I. Pitts <rip@cs.bu.edu>
 * Last Modified: March 7, 2000
 *  *         Topic: Stack - Array Implementation
 *   * ----------------------------------------------------------------
 *    *
 *     * This is an array implementation of a character stack.
 *      */

#include <stdio.h>
#include <stdlib.h>  /* for dynamic allocation */
#include "stack.h"       
#include "board.h"

/************************ Function Definitions **********************/

void StackInit(stackT *stackP, int maxSize)
{
  struct board **newContents;

  /* Allocate a new array to hold the contents. */

  newContents = (struct board **)malloc(sizeof(struct board *) * maxSize);

  if (newContents == NULL) {
    fprintf(stderr, "Insufficient memory to initialize stack.\n");
    exit(1);  /* Exit, returning error code. */
  }

  stackP->contents = newContents;
  stackP->maxSize = maxSize;
  stackP->top = -1;  /* I.e., empty */
}

void StackDestroy(stackT *stackP)
{
  /* Get rid of array. */
  free(stackP->contents);

  stackP->contents = NULL;
  stackP->maxSize = 0;
  stackP->top = -1;  /* I.e., empty */
}

int StackPush(stackT *stackP, struct board *b)
{
  if (StackIsFull(stackP)) {
    return -1;  /* Exit, returning error code. */
  }

  /* Put information in array; update top. */

  stackP->contents[++stackP->top] = b;
	return 0;
}

struct board* StackPop(stackT *stackP)
{
  if (StackIsEmpty(stackP)) {
    fprintf(stderr, "Can't pop element from stack: stack is empty.\n");
    exit(1);  /* Exit, returning error code. */
  }

  return stackP->contents[stackP->top--];
}

int StackIsEmpty(stackT *stackP)
{
  return stackP->top < 0;
}

int StackIsFull(stackT *stackP)
{
  return stackP->top >= stackP->maxSize - 1;
}
void stack_free_all(stackT *stackP){
while(!StackIsEmpty(stackP)){
	struct board *b = StackPop(stackP);
	free(b);
}
StackDestroy(stackP);
}
