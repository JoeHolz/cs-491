#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#ifndef BOARD_H
#define BOARD_H
#define BOARD_SIZE 9

bool debug;
int board [BOARD_SIZE];
struct board{
	int tiles[BOARD_SIZE];
	char move;
	struct board *parent;
	int distance;
};


int boardRate(struct board b);
int mDist(struct board b, int value);
bool isValid();
bool structIsValid(struct board b);
int getChildren(struct board *b, struct board **children,int avoidRepeats);
bool isSolved();
bool structIsSolved(struct board b);
void printBoard(struct board b);
int boardEquals(struct board* a, struct board* b);
int hasRepeats(struct board*b);
#endif
