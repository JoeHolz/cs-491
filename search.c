#include "board.h"
#include "queue.h"
#include "priority_queue.h"


// take initial state, return the solved state.
void breadthFirst(struct board* initial, int avoidRepeats){
	//vars to hold state counts
	int expandedStates = 0;
	int storedStates = 0;
	//initialize queue we will use for breadth first search`
	queue q;
	queue discard;
	init_queue(&q);
	init_queue(&discard);

	//store initial state as a potential solution
	struct board* solution = initial;

	//we need a small array to hold the 4 potential children in the search loop
	struct board* children[4];
	//main search loop.  keep going until we've found a solution.
	int noErr = 1;
	while(!structIsSolved(*solution) && noErr){
	
		//this retrieves children state;
		int count = getChildren(solution, children, avoidRepeats);
		//increment the number of expanded states
		expandedStates++;
		//put children in queue;
		for(int i = 0; i < count; i++){
			//put the child nodes in the queue, and also check if there was an overflow error.
			if(enqueue(&q, children[i]) == -1){
				noErr = 0;
			}
			//one more state in the queue
			storedStates++;
		}

		//check next node in queue
		solution = dequeue(&q);
		//one less state in the queue
		storedStates--;
		enqueue(&discard, solution);
if(debug){
printf("about to expand board:\n");
printBoard(*solution);
}
	}

if(debug){
printf("%d states were expanded\n%d states were stored in the queue\n", expandedStates, storedStates);
}

	//there was an overflow error
	if(!noErr){
		printf("ran out of queue space during search\n");
	}else if(debug){
		printf("\nbreadth first solution(reversed):\n\n");
		
		while(solution->parent != NULL){
			printBoard(*solution);
			printf("moved %c\n", solution->move);
			solution = solution->parent;
		}
		printf("initial state:\n");
		printBoard(*solution);
	}else{
		//normal output
		printf("breadth first search");
		if(avoidRepeats){
			printf(", repeats avoided:\n");
		}else{
			printf(":\n");
		}
		printf("(");
		while(solution->parent != NULL){
			printf("%c ", solution->move);
			solution = solution->parent;
		}
		printf(")\n");
	}

	//free the two queues now that we're all done.
	free_all(&q);
	free_all(&discard);
}

void bestFirst(struct board* initial){
	//avoid repeated states by default
	int avoidRepeats = 1;
	//vars to hold state counts
	int expandedStates = 0;
	int storedStates = 0;
	//initialize queue we will use for best first search`
	priority_queue q;
	queue discard;
	pq_init(&q);
	init_queue(&discard);

	//store initial state as a potential solution
	struct board* solution = initial;
	//check distance from solution.
	solution->distance = boardRate(*initial);

	//we need a small array to hold the 4 potential children in the search loop
	struct board* children[4];
	//main search loop.  keep going until we've found a solution.
	int noErr = 1;
	while(!structIsSolved(*solution) && noErr){
	
		//this retrieves children state;
		int count = getChildren(solution, children, avoidRepeats);
		//increment the number of expanded states
		expandedStates++;
		//put children in queue;
		for(int i = 0; i < count; i++){
			//first evaluate the distance of the child
			children[i]->distance = boardRate(*(children[i]));
			//put the child nodes in the queue, and also check if there was an overflow error.
			if(pq_insert(&q, children[i]) == -1){
				noErr = 0;
			}
			//one more state in the queue
			storedStates++;
		}

		//check next node in queue
		solution = extract_min(&q);
		//one less state in the queue
		storedStates--;
		enqueue(&discard, solution);
if(debug){
printf("about to expand board:\n");
printBoard(*solution);
}
	}

if(debug){
printf("%d states were expanded\n%d states were stored in the queue\n", expandedStates, storedStates);
}

	//there was an overflow error
	if(!noErr){
		printf("ran out of queue space during search\n");
	}else if(debug){
		printf("\nbest first solution(reversed):\n\n");
		
		while(solution->parent != NULL){
			printBoard(*solution);
			printf("moved %c\n", solution->move);
			solution = solution->parent;
		}
		printf("initial state:\n");
		printBoard(*solution);
	}else{
		//normal output
		printf("best first search:\n");
		printf("(");
		while(solution->parent != NULL){
			printf("%c ", solution->move);
			solution = solution->parent;
		}
		printf(")\n");
	}

	//free the two queues now that we're all done.
	free_pq(&q);
	free_all(&discard);
}



